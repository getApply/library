<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Library  Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('LIBRARY_NAME', 'Library'),

    /*
    |--------------------------------------------------------------------------
    | Library namespace
    |--------------------------------------------------------------------------
    |
    | This is the default namespace for the apply packages
    |
    */
    'namespace' => 'Packages',

    /*
    |--------------------------------------------------------------------------
    | Packages path
    |--------------------------------------------------------------------------
    |
    | This path used for save the generated package. This path also will added
    | automatically to list of scanned folders.
    |
    */
    'path'  => base_path('common/packages'),

    /*
    |--------------------------------------------------------------------------
    | Library´s scan
    |--------------------------------------------------------------------------
    */
    'scan' => [
        'filename' => 'apply.json',
        'folder' => base_path('common'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Packages Stubs
    |--------------------------------------------------------------------------
    |
    | Default package stubs.
    |
    */
    'stubs' => [
        'path' => realpath(APPLY_LIBRARY_PATH.'/src/Console/stubs'),
        'files'  => [
            'apply'                 => 'apply.json',
            'license'               => 'LICENCE.md',
            'composer'              => 'composer.json',
            'loader'                => 'src/Setup/loader.php',
            'RouteServiceProvider'  => 'src/Providers/RouteServiceProvider.php',
            'AppServiceProvider'    => 'src/Providers/AppServiceProvider.php',
            'Controller'            => 'src/Http/controllers/Controller.php',
        ],
    ],
];
