<?php

if (! function_exists('package')) {
    /**
     * Get / set the specified package value.
     *
     * If an array is passed as the key, we will assume you want to set an array of values.
     *
     * @param  array|string $key
     * @param bool $default
     * @return mixed
     */
    function package($key = null, $default = true)
    {
        return new \Apply\Library\Package();
    }
}
