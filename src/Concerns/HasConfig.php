<?php

namespace Apply\Library\Concerns;

use Illuminate\Support\Str;

trait HasConfig
{
    /**
     * Config.
     *
     * @param null $key
     * @return \Illuminate\Config\Repository|mixed
     */
    public function config($key = null)
    {
        $class = Str::snake(class_basename($this));
        $driver = $this->driver ?? $this->getAttribute('type');
        $config = $class == 'package' ? 'library' : $class;

        return  $key ? config($driver ?? $config.'.'.$key) : config($driver ?? $config);
    }

}
