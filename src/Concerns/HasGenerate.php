<?php

namespace Apply\Library\Concerns;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Apply\Library\Support\Stub;

trait HasGenerate
{
    /**
     * Generate the package.
     * @param $name
     * @return mixed
     */
    public function generate($name)
    {
        $filesystem = new Filesystem();
        $name = strtolower($name);
        $package = Str::studly($name);

        $driver = $this->driver();
        $drivers = Str::studly(Str::plural($driver));


        $itemPath = $this->path($name);
        $namespace = $this->namespace($package);

        $uuid = (string) Str::uuid();
        $date = date('Y-m-d');

        if ($filesystem->isDirectory($itemPath))
            return ['status' => 'error', 'message' => 'Sorry "'.$package.'" Package Folder Already Exist !!!', 'id' => $uuid];

        foreach ($this->config('stubs.files') as $key => $value)
            Stub::createFromPath($this->config('stubs.path').'/'.$key.'.stub',
                compact(['name', 'driver', 'drivers', 'package', 'namespace', 'date', 'uuid']))
                ->saveTo($itemPath, $value);

        $this->lock()->scan();

        return ['status' => 'success', 'message' => 'Package created successfully.', 'id' => $uuid];
    }
}
