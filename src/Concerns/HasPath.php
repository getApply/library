<?php

namespace Apply\Library\Concerns;

trait HasPath
{
    /**
     * Path.
     *
     * @param null $key
     * @return mixed|string
     */
    public function path($key = null)
    {
        $path =  $this->config('path');

        if ($this->exists){

            $path =  (array)$this->getFile();
            $path = dirname($path['pathname']);
        }

        if ($key) {
            return $path.'/'.$key;
        }

        return $path;
    }
}
