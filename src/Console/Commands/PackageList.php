<?php

namespace Apply\Library\Console\Commands;

use Illuminate\Console\Command;

class PackageList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'package:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all Packages';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->table(['Name', 'Driver', 'Status', 'Core', 'Path'], $this->getRows());
    }

    /**
     * Get table rows.
     *
     * @return array
     */
    public function getRows()
    {
        $rows = [];

        foreach (package()->read() as $package) {
            $rows[] = [
                $package->name,
                $package->driver(),
                $package->active ? 'Enabled' : 'Disabled',
                $package->core ? 'true' : 'false',
                $package->path(),
            ];
        }
        return $rows;
    }

}
