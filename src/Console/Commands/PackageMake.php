<?php

namespace Apply\Library\Console\Commands;

use Illuminate\Console\Command;

class PackageMake extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:package {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $module = package()->generate($this->argument('name'));

        if ($module['status'] == 'error')
            $this->error($module['message']);

        elseif($module['status'] == 'success')
            $this->info($module['message']);
    }
}
