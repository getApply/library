<?php

namespace Apply\Library;

use Illuminate\Support\Collection;
use Illuminate\Support\Traits\Macroable;

class Package
{
    use Macroable;
    use Concerns\HasDriver,
        Concerns\HasAttributes,
        Concerns\HasLock,
        Concerns\HasMagic,
        Concerns\HasNamespace,
        Concerns\HasPath,
        Concerns\HasFile,
        Concerns\HasKey,
        Concerns\HasComposer,
        Concerns\HasGenerate,
        Concerns\HasConfig;

    /**
     * Indicates if the item exists.
     *
     * @var bool
     */
    public $exists = false;

    /**
     * Package constructor.
     *
     * @param  array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->syncOriginal();
    }

    /**
     * Create a new instance of the given item.
     *
     * @param  array  $attributes
     * @param  bool  $exists
     * @return static
     */
    public function newInstance($attributes = [], $exists = false)
    {
        $model = new static((array) $attributes);
        $model->exists = $exists;
        return $model;
    }

    /**
     * Create a new item instance that is existing.
     *
     * @param  array  $attributes
     * @return static
     */
    public function newFromBuilder($attributes = [])
    {
        $model = $this->newInstance([], true);

        $model->setFile($attributes['file']);
        $model->setComposer($attributes['composer']);
        $model->setRawAttributes((array) $attributes['data'], true);

        return $model;
    }

    /**
     * Read the data from the store.
     *
     * @param bool $instance
     * @return mixed
     */
    public function read($instance = true)
    {
        $items = [];

        $finder = $this->lock();

        foreach ($finder->getPackages() as $package)
        {
            $item = $this->formatItem($package, $instance);

            !$this->driver
                ? $items[] = $item
                : $package['type'] != $this->driver ?: $items[] = $item ;
        }

        return new Collection($items);
    }

    /**
     * Format the data from item.
     *
     * @param $package
     * @param bool $instance
     * @return array
     */
    public function formatItem($package, $instance = true)
    {
        return $item = $this->item($package, $instance);
    }

    /**
     * Format the default data from item.
     *
     * @param null $package
     * @param bool $instance
     * @return mixed|null
     */
    public function item($package = null, $instance = false)
    {
        $item = $package['data'];
        if (array_key_exists('alias' , $item) && $item['alias'] == null){
            $item['alias'] = $item['type'].'-'.$item['name'];
        }

        if (array_key_exists('core', $item) && $item['core']){
            $item['active'] = $item['core'];
        }

        $package['data'] = $item;
        return $instance ? $this->newFromBuilder($package) : $item;
    }

}
