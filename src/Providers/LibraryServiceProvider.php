<?php

namespace Apply\Library\Providers;

use Apply\Library\Support\Helper;
use Apply\Library\Console\Commands\PackageList;
use Apply\Library\Console\Commands\PackageMake;
use Apply\Library\Package;
use Apply\Library\Support\Autoload;
use Illuminate\Support\ServiceProvider;

class LibraryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (! defined('APPLY_LIBRARY_PATH')) {
            /*
             * Get the path to the Apply library folder.
             */
            define('APPLY_LIBRARY_PATH', realpath(__DIR__.'/../../'));
        }

        $configPath = realpath(APPLY_LIBRARY_PATH . '/config/library.php');
        $this->mergeConfigFrom($configPath, 'library');
        $this->registerCommands();
        $this->makeDirectory();

        Helper::autoload(realpath(APPLY_LIBRARY_PATH . '/helpers'));
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerAutoload();
    }

    /**
     * Register Library´s.
     *
     * @return void
     */
    public function registerAutoload()
    {
        $packages = new Package();

        $items = $packages->read()->filter(function ($item) {
            return $item->active == true;
        });

        foreach ($items as $package)
        {
            foreach ((array)$package->composer('autoload.psr-4') as $class => $src)
            {
                Autoload::register($class, $package->path($src));
            }
        }

        foreach ($items as $package)
        {
            $method = $package->namespace('Setup\Loader');
            $this->app->register(new $method($this->app, $package));
        }
    }

    /**
     * Make Directory if no exist.
     *
     * @return void
     */
    public function makeDirectory()
    {
        if (!file_exists(config('library.path')))
            mkdir(config('library.path'), 0755, true);
    }

    /**
     * Register Commands.
     *
     * @return void
     */
    public function registerCommands()
    {
        $this->commands([
            PackageMake::class,
            PackageList::class
        ]);

    }
}
