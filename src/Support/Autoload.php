<?php

namespace Apply\Library\Support;

class Autoload
{
    public static function register($class, $path)
    {
        $composer = require(base_path('vendor/autoload.php'));

        if (! array_key_exists($class, $composer->getClassMap())) {
            $composer->addPsr4($class, $path);
             $composer->register();
        }
    }
}
